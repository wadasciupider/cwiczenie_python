#!/usr/bin/python
import math
import numpy


class Complex(object):

    def __init__(self, real, imag):
        self.real = real
        self.imag = imag

    def __str__(self):
        return str(self.real)+("+" if numpy.sign(self.imag)==1 else "")+str(self.imag)+"i"

    def __add__(self, rhs):
        return Complex(self.real + rhs.real, self.imag + rhs.imag)
   
    def __mul__(self,rhs):
        return Complex(self.real*rhs.real - rhs.imag*self.imag, self.real*rhs.imag + self.imag*rhs.real)
    
    def __sub__(self,rhs):
        return Complex(self.real - rhs.real, self.imag - rhs.imag)

    def __abs__(self):
        return  math.sqrt(self.real**2 + self.imag**2)

