#!/usr/bin/python
import unittest
from complex import Complex


class TestComplexMethods(unittest.TestCase):

    def test_add(self):
        self.A = Complex(5,10)
        self.B = Complex(1,2)
        self.assertEqual((self.A+self.B).real, 6)
        self.assertEqual((self.A+self.B).imag, 12)

    def test_sub(self):
        self.A =Complex(5,10)
        self.B = Complex(1,2)
        self.assertEqual((self.A-self.B).real,4)
        self.assertEqual((self.A-self.B).imag,8)
    
    def test_abs(self):
	    self.A = Complex(3,4)
	    self.assertEqual(abs(self.A),5)

    def test_mul(self):
        self.A = Complex(1,2)
        self.B = Complex(3,0)
        self.assertEqual((self.A*self.B).real, 3)
        self.assertEqual((self.A*self.B).imag, 6)
	

if __name__ == '__main__':
    unittest.main()
